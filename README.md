# Coursework Summary

## Table of Contents

- [Section Descriptions](#section-descriptions)
- [Course Descriptions](#course-descriptions)
  - [Business](#business)
  - [CEIS](#ceis)
  - [CIS](#cis)
  - [Math](#math)
  - [SEC](#sec)


## Section Descriptions

 ### Business Business Business
 Used as a container for business and soft-skill courses that do not fit in any other category.


 ### CEIS (Engineering Technology and Information Sciences)
 Coursework is focused on problem solving and soft skills.


 ### CIS (Computer Information Systems)
 Coursework is focused on technical skills.


 ### Math
 Contains mathematics.

 ### SEC (Information Systems Security)
 Coursework covers the vulnerabilities and securing of information systems from physical, social, and computational attacks and negligence.


## Course Descriptions
Microsoft Word is used in all of my courses.

 ### Business

  #### BUSN 115 (Introduction to Business and Technology)

   ##### Technologies Used:
   Excel - This was not required for this course. Tangentially related to a small assignment, I tried performing data analysis on global GDP and population data. I found this to be quite difficult as region UIDs are not well standardized between organizations, and the table I needed to convert them was only available behind a paywall.

   ##### Work Description:
   Course was awful. Barely passed. After a program update, this course no longer counted towards my degree.

   ##### Course Description:
   This course introduces business and the environments in which businesses operate. Students examine the roles of major functional areas of business and interrelationships among them. Organizational theories and techniques are examined and economic, cultural, political,and technological factors affecting business organizations are evaluated.


  #### COLL 148 (Critical Thinking and Problem-Solving)

   ##### Technologies Used:
   None

   ##### Work Description:
   Discussed time and stress management with other students.

   ##### Course Description:
   This course focuses on identifying and articulating skills needed for academic and professional success. Coursework provides instruction and practice in critical thinking and problem solving through analysis of critical reading and reasoning, as well as through examination of problem-solving methodologies. Students learn to work in teams, to identify and resolve problems, and to use research effectively to gather and evaluate relevant and useful information.


 ### CEIS
  
  #### CEIS 100 (Introduction to Engineering Technology and Information Sciences)

   ##### Technologies Used:
   Python\
   Excel\
   MIT Scratch\
   HTML\
   Windows CLI

   ##### Work Description:
   Coded introductory level Python script; used excel as a calculator for a math proof; coded a Scratch animation; coded an ugly website; interpreted output from networking related terminal commands

   ##### Course Description:
   This course introduces basics of networking, programming logic and electrical engineering technology concepts. Topics include the importance of ethics and communications in the engineering world, as well as the benefits of belonging to a professional organization. In the lab, students gain experience in problem-solving and completing lab reports. They also create portfolios and plan which courses they will take while at DeVry.


  #### CEIS 295 (Data Structures and Algorithms)

   ##### Technologies Used:
   Visual C++

   ##### Work Description:
   Implemented basic data structures and algorithms.

   ##### Course Description:
   This course introduces structures that allow efficient organization and data retrieval, frequently used algorithms and basic techniques for modeling, as well as understanding and solving algorithmic problems. Arrays and linked lists, hash tables and associative arrays, sorting and selection, priority queues, sorted sequences, trees, graph representation, graph traversal, and graph algorithms are covered.


 ### CIS

  #### CIS 170C (Programming with Lab)

   ##### Technologies used: 
   Visual C++

   ##### Work Description:
   Ported a previously made hangman game from Python to C++; learned how to manipulate the standard streams; learned how to implement header files

   ##### Course Description:
   This course introduces basics of coding programs from program specifications, including use of an integrated development environment (IDE), language syntax, as well as debugger tools and techniques. Students also learn to develop programs that manipulate simple data structures, such as arrays, as well as different types of files. C++.Net is the primary programming language used.


  #### CIS 206 (Architecture and Operating Systems With Lab)

   ##### Technologies Used:
   Windows CLI\
   Linux Bash\
   LabSim - provided a digital environment to practice setting up system hardware and operating software

   ##### Work Description:
   Researched computer parts; used system terminal to perform basic tasks and maintenance; created scripts to automate afore mentioned tasks

   ##### Course Description:
   This course introduces operating system concepts by examining a number of operating systems, such as Windows and Linux. Students also study typical desktop system hardware, architecture, and configuration. Networking and security basics are introduced.


  #### CIS 247C (Object-Oriented Programming with Lab)

   ##### Technologies Used:
   Visual C++\
   Lucidchart

   ##### Work Description:
   Improved usage of header and project files; increased familiarity with streams; learned about object referencing and improved control flow; created UML charts

   ##### Course Description:
   This course introduces object-oriented programming concepts including objects, classes, encapsulation, polymorphism and inheritance. Using an object-oriented programming language,students design, code, test and document object-oriented applications. C++ is the primary programming language used.

  
  #### CIS 321 (Structured Analysis and Design)

   ##### Technologies Used:
   Lucidchart\
   Adobe XD

   ##### Work Description:
   Led a team of two other students in designing a fake information system to manage employee information and benefits for a provided scenario. Scenario problems and opportunities were assessed. Various diagrams and a mock UI was made. The work was then prepared for a final presentation.

   ##### Course Description:
   This course introduces the systems analysis design process using information systems methodologies and techniques to analyze business activities and solve problems. Students learn to identify, define, and document business problems and then develop information system models to solve them.

  
  #### CIS 336 (Introduction to Database with Lab)

   ##### Technologies Used:
   E.D.U.P.E. Web Server\
   MySQL 5.5\
   Lucidchart\
   Mockaroo

   ##### Work Description:
   Designed, implemented, and tested a book management system for a fake, multi-branch library. The design phase included creating a scenario, an entity relationship diagram, and a data dictionary. Testing primarily consisted of flooding the database with mock data, generated using Mockaroo; unfortunately, I was not able to test the system to the extent I had hoped due to how Mockaroo interprets single-columned tables and MySQL handles insertion errors.

   ##### Course Description:
   This course introduces the concepts and methods fundamental to database development and use, including data analysis, modeling, and Structured Query Language (SQL). Students also explore basic functions and features of a database management system (DBMS), with an emphasis on the relational model.


 ### Math
  
  #### MATH 221 (Statistics for Decision Making)

   ##### Technologies Used:
   Excel

   ##### Work Description:
   Completed guided statistical analyses.

   ##### Course Description:
   This course provides tools used for statistical analysis and decision making in business. The course includes both descriptive statistics and inferential concepts used to draw conclusions about a population. Research techniques, such as sampling and experiment design, are included for both single and multiple sample groups.


 ### SEC

  #### SEC 280 (Principles of Information System Security)

   ##### Technologies Used:
   CrypTool\
   Bulk Extractor

   ##### Work Description:
   Completed guided exercises and research in encryption and network security.

   ##### Course Description:
   This course provides a broad overview of information systems security in organizations. Topics include security concepts and mechanisms, mandatory and discretionary controls, basic cryptography and its applications, intrusion detection and prevention, information-systems assurance, and anonymity and privacy. Various types of controls used in information systems, as well as security issues surrounding the computer and computer-generated data, are also addressed.
